#!/bin/sh
mkdir bundle
cd bundle
git clone https://github.com/scrooloose/syntastic.git
git clone https://github.com/majutsushi/tagbar.git
git clone https://github.com/scrooloose/nerdtree.git
wget https://gist.githubusercontent.com/masaakif/414375/raw/71cf6a760578c2d484a835a6195facd7fb1368c6/grep_menuitem.vim
mkdir ../nerdtree_plugin
mv grep_menuitem.vim ../nerdtree_plugin/
git clone https://github.com/bling/vim-airline 
git clone https://github.com/kien/ctrlp.vim.git 
git clone https://github.com/mattn/emmet-vim.git
git clone https://github.com/Lokaltog/vim-distinguished.git
mv vim-distinguished/colors ../
git clone https://github.com/leafgarland/typescript-vim.git
git clone https://github.com/neoclide/coc.nvim.git
git clone https://github.com/pangloss/vim-javascript
git clone https://github.com/MaxMEllon/vim-jsx-pretty
